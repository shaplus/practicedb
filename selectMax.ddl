select max(SKozhenivskyi.products_has_stores.count)
from SKozhenivskyi.products_has_stores;

explain select store_idstore, sum(count) as sum
from (select * from products force index for join (index1) where groupsp_id = 2) as prod
    inner join products_has_stores phs  on prod.id = phs.product_id
    inner join groupsp g on prod.groupsp_id = g.id
group by store_idstore
order by sum limit 1;

explain select store_idstore, sum(count) as sum from stores
inner join products_has_stores phs on stores.id = phs.store_idstore
inner join products p on phs.product_id = p.id
where groupsp_id = 2
group by store_idstore order by sum desc limit 1;


select store_idstore
        from products
                 inner join products_has_stores phs on products.id = phs.product_id
                 straight_join groupsp g on products.groupsp_id = g.id
        where groupsp_id = 2
        group by store_idstore;


(select products.id, groupsp_id
 from products
          inner join groupsp g on products.groupsp_id = g.id
 where products.groupsp_id = 2)
as igi on products_has_stores.product_id = igi.id;
select products.id, groupsp_id
from products
         inner join groupsp g on products.groupsp_id = g.id
order by products.id;

select store_idstore, groupsp_id, sum(count) as sum
from (select *
      from products_has_stores
               inner join (select products.id, groupsp_id
                           from products
                                    inner join groupsp g on
                               products.groupsp_id = g.id
                           order by products.id) as igi on products_has_stores.product_id = igi.id)
         as `tab`
group by store_idstore, groupsp_id;

select store_idstore, sum
from (select store_idstore, groupsp_id, sum(count) as sum
      from (select *
            from products_has_stores
                     inner join (select products.id, groupsp_id
                                 from products
                                          inner join groupsp g on
                                     products.groupsp_id = g.id
                                 where groupsp_id = 2) as igi on products_has_stores.product_id = igi.id)
               as `tab`
      group by store_idstore, groupsp_id) as tab2
where groupsp_id = 2
order by sum desc;


create index index1 on products (groupsp_id, id);
create index index2 on products_has_stores (store_idstore, product_id);

show index from products;
alter table products disable keys;
alter table products_has_stores disable keys;

alter table products enable keys ;
alter table products_has_stores enable keys;

select count(*) from products

