package com.myComp.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class LetterValidator implements ConstraintValidator<LettersValidator, String> {

    private int vowelLet;
    @Override
    public void initialize(LettersValidator constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        vowelLet = constraintAnnotation.vowelsLet();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        int count = 0;
        for (int i=0 ; i<s.length() && count<vowelLet; i++){
            char ch = s.charAt(i);
            if(ch == 'a'|| ch == 'e'|| ch == 'i' ||ch == 'o' ||ch == 'u' ){
                count ++;
            }
        }
        return count >= vowelLet;
    }
}
