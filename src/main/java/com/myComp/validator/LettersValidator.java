package com.myComp.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LetterValidator.class)
public @interface LettersValidator {
    int vowelsLet() default 3;
    String message() default "Not enought vowel letters in the word";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
