package com.myComp;

import java.sql.*;

public class DBManager {


    private static DBManager instance;
    private static final String URL = "jdbc:mariadb://localhost:3307/prods";

    private DBManager() {

    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void testConnection() {
        try {
            Connection con = DriverManager.getConnection(URL, "root", "root");
            System.out.println("Good");
        } catch (SQLException e) {
            System.out.println("Bad");
            throw new RuntimeException(e);
        }
    }

    public void testInsert() {
        try (Connection con = DriverManager.getConnection(URL, "root", "root");) {
            String statement = "insert into skozhenivskyi. values (default, 'store1')";
            PreparedStatement preparedStatement = con.prepareStatement(statement);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

}
