package com.myComp;

import com.myComp.enums.ProductGroup;
import com.myComp.enums.Stores;
import com.myComp.validator.LettersValidator;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public class ProductDTO {
    @NotNull (message = "Product name can't be null")
    @Length (min = 3, max = 50, message = "Product name length must be between 3 and 50")
    @LettersValidator(vowelsLet = 3)
    private final String product_name;
    @NotNull
    private final ProductGroup product_group;
    @NotNull
    private final Stores store;
    @Min(0)
    @Max(1000)
    private int count;

    public ProductDTO(String product_name, ProductGroup product_group, Stores store, int count) {
        this.product_name = product_name;
        this.product_group = product_group;
        this.store = store;
        this.count = count;
    }

    public String getProduct_name() {
        return product_name;
    }

    public ProductGroup getProduct_group() {
        return product_group;
    }

    public Stores getStore() {
        return store;
    }

    public int getCount() {
        return count;
    }



    public void setCount(int count) {
        this.count = count;
    }
}
