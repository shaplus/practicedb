package com.myComp;

import com.myComp.enums.ProductGroup;
import com.myComp.enums.Stores;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.LoggerFactory;


import java.sql.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DBFiller {
    private static final Logger logger = LoggerFactory.getLogger(DBFiller.class);
    private final static Random random = new Random();
    private final static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    //private static final String URL = "jdbc:mariadb://localhost:3307/skozhenivskyi";
    private static final String USER = "user";
    private static final String PASSWORD = "resu";
    private static final String URL = "jdbc:mariadb://46.119.86.45:17306/SKozhenivskyi";
    private static final int BATCH_SIZE = 5000;

    public static void main(String[] args) {
        String todo = System.getProperty("todo");
        String value = System.getProperty("value");
        String insertGS = System.getProperty("insertGS");
        if (todo != null && todo.toLowerCase().equals("insert")) {
            int inputSize = 5000;
            try {
                inputSize = Integer.parseInt(value);
                if (insertGS != null && insertGS.toLowerCase().equals("true")) {
                    insert(inputSize, true);
                } else {
                    insert(inputSize, false);
                }

            } catch (NumberFormatException | NullPointerException e) {
                logger.warn("Input value [" + value + "] is not a number", e);
            }
        } else if (todo != null && todo.toLowerCase().equals("select")) {
            select(value);
        } else {
            logger.warn("You haven`t specified todo variable");
        }
    }

    public static void insert(int intCount, boolean insertGS) {
        long progStart = System.currentTimeMillis();
        Map<ProductGroup, Integer> prodGroupsMap = null;
        Map<Stores, Integer> storesMap = null;
        if (insertGS) {
            prodGroupsMap = insertGroups();
            storesMap = insertStores();
        } else {
            prodGroupsMap = getGroups();
            storesMap = getStore();
        }
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        for (int j = 0; j < intCount / BATCH_SIZE; j++) {
            int finalJ = j;
            Map<Stores, Integer> finalStoresMap = storesMap;
            Map<ProductGroup, Integer> finalProdGroupsMap = prodGroupsMap;
            Runnable runnable = () -> {
                try {
                    Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
                    con.setAutoCommit(false);
                    con.prepareStatement("set transaction isolation level read uncommitted").execute();
                    String statement = "insert into SKozhenivskyi.products (product_name, groupsp_id) value (?, ?)";
                    ProductDTO[] productDTOS = new ProductDTO[BATCH_SIZE];
                    int i = 0;
                    int[] keys = new int[BATCH_SIZE];
                    long start = System.currentTimeMillis();
                    PreparedStatement preparedStatement = con.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);
                    while (i < BATCH_SIZE) {
                        ProductDTO productDTO = productFactory();
                        boolean isVal = isValid(productDTO);
                        if (isVal) {
                            productDTOS[i] = productDTO;
                            i++;
                            preparedStatement.setString(1, productDTO.getProduct_name());
                            preparedStatement.setInt(2, finalProdGroupsMap.get(productDTO.getProduct_group()));
                            preparedStatement.addBatch();
                        }
                    }
                    logger.info("Creating time: " + (System.currentTimeMillis() - start) + " ms. [" + finalJ * BATCH_SIZE + "-" + (finalJ + 1) * BATCH_SIZE + "].");
                    start = System.currentTimeMillis();
                    preparedStatement.executeBatch();
                    preparedStatement.clearBatch();
                    con.commit();
                    ResultSet rsKeys = preparedStatement.getGeneratedKeys();
                    int a = 0;
                    while (rsKeys.next()) {
                        keys[a] = rsKeys.getInt(1);
                        a++;
                    }
                    ResultSet rs = preparedStatement.getGeneratedKeys();
                    int k = 0;
                    con.commit();
                    String secondState = "insert into SKozhenivskyi.products_has_stores (store_idstore, product_id, count) value (?,?,?)";
                    preparedStatement = con.prepareStatement(secondState);
                    for (int l = 0; l < productDTOS.length; l++) {
                        preparedStatement.setInt(1, finalStoresMap.get(productDTOS[l].getStore()));
                        preparedStatement.setInt(2, keys[l]);
                        preparedStatement.setInt(3, productDTOS[l].getCount());
                        preparedStatement.addBatch();
                    }
                    preparedStatement.executeBatch();
                    preparedStatement.clearBatch();
                    logger.info("Execution time: " + (System.currentTimeMillis() - start) + " ms. [" + finalJ * BATCH_SIZE + "-" + (finalJ + 1) * BATCH_SIZE + "].");
                    con.commit();
                    con.close();
                    preparedStatement.close();
                } catch (SQLException | NullPointerException e) {
                    e.printStackTrace();
                }
            };
            executorService.submit(runnable);
        }
        shutdownAndAwaitTermination(executorService);
        logger.info("Program time: " + (System.currentTimeMillis() - progStart) + " ms.");
    }

    public static Map<ProductGroup, Integer> getGroups() {
        Map<ProductGroup, Integer> result = new HashMap<>();
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement preparedStatement = con.prepareStatement("select * from SKozhenivskyi.groupsp")) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result.put(EnumUtils.getEnum(ProductGroup.class, rs.getString(2)), rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<ProductGroup, Integer> insertGroups() {
        Map<ProductGroup, Integer> result = new HashMap<>();
        ProductGroup[] productGroups = ProductGroup.values();
        int[] indexes = new int[productGroups.length];
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement preparedStatement = con.prepareStatement("insert into SKozhenivskyi.groupsp (group_name) values (?)", Statement.RETURN_GENERATED_KEYS)) {
            con.prepareStatement("alter table SKozhenivskyi.groupsp AUTO_INCREMENT = 0").execute();
            con.prepareStatement("alter table SKozhenivskyi.products AUTO_INCREMENT = 0").execute();
            con.prepareStatement("start transaction ").addBatch();
            for (int i = 0; i < productGroups.length; i++) {
                preparedStatement.setString(1, productGroups[i].name());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int i = 0;
            while (rs.next()) {
                indexes[i] = rs.getInt(1);
                i++;
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < productGroups.length; i++) {
            result.put(productGroups[i], indexes[i]);
        }
        return result;
    }

    public static Map<Stores, Integer> insertStores() {
        Map<Stores, Integer> result = new HashMap<>();
        Stores[] stores = Stores.values();
        int[] indexes = new int[stores.length];
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement preparedStatement = con.prepareStatement("insert into SKozhenivskyi.stores (store) values (?)", Statement.RETURN_GENERATED_KEYS)) {
            con.prepareStatement("alter table SKozhenivskyi.stores AUTO_INCREMENT = 0").execute();
            con.prepareStatement("start transaction ").addBatch();
            for (int i = 0; i < stores.length; i++) {
                preparedStatement.setString(1, stores[i].name());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int i = 0;
            while (rs.next()) {
                indexes[i] = rs.getInt(1);
                i++;
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < stores.length; i++) {
            result.put(stores[i], indexes[i]);
        }
        return result;
    }

    public static Map<Stores, Integer> getStore() {
        Map<Stores, Integer> result = new HashMap<>();
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement preparedStatement = con.prepareStatement("select * from SKozhenivskyi.stores")) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result.put(EnumUtils.getEnum(Stores.class, rs.getString(2)), rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void select(String group) {
        long progStart = System.currentTimeMillis();
        Map<ProductGroup, Integer> groups = getGroups();
        Map<Stores, Integer> stores = getStore();
        if (EnumUtils.isValidEnum(ProductGroup.class, group.toUpperCase())) {
            String statement = "select store_idstore, sum(count) as sum from products " +
                    "inner join products_has_stores phs on products.id = phs.product_id " +
                    "inner join groupsp g on products.groupsp_id = g.id " +
                    "where groupsp_id = ? group by store_idstore order by sum desc limit 1;";
            try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
                 PreparedStatement preparedStatement = con.prepareStatement(statement)) {
                int groupId = groups.get(EnumUtils.getEnum(ProductGroup.class, group.toUpperCase()));
                preparedStatement.setInt(1, groupId);
                ResultSet rs = preparedStatement.executeQuery();
                preparedStatement.executeUpdate();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    int count = rs.getInt(2);
                    logger.info("The " + group.toUpperCase() + " product group is the most available in " + getStoreName(id) + " [" + count + "].");
                } else {
                    logger.info("Something went wrong");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            logger.warn("No such category : [" + group + "].");
        }
        logger.info("Program time: " + (System.currentTimeMillis() - progStart) + " ms.");
    }

    public static boolean isValid(ProductDTO product) {
        Set<ConstraintViolation<ProductDTO>> validSet = validator.validate(product);
        boolean isValid = validSet.isEmpty();
        if (!isValid) logger.trace(validSet.iterator().next().getMessage());
        return isValid;
    }


    public static ProductDTO productFactory() {
        String name = RandomStringUtils.randomAlphabetic(3, 50).toLowerCase(Locale.ROOT);
        ProductGroup group = ProductGroup.values()[random.nextInt(ProductGroup.getSize())];
        Stores store = Stores.values()[random.nextInt(Stores.getSize())];
        int count = random.nextInt(1000);
        return new ProductDTO(name, group, store, count);
    }

    static void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown();
        try {

            if (!pool.awaitTermination(10, TimeUnit.HOURS)) {
                pool.shutdownNow();
                if (!pool.awaitTermination(10, TimeUnit.HOURS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            pool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public static String getStoreName(int id) {
        Map<Stores, Integer> stores = getStore();
        final String[] result = {""};
        stores.forEach((k, v) -> {
            if (v == id) {
                result[0] = k.name();
            }
        });
        return result[0];
    }

}
