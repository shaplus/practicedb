
CREATE DATABASE IF NOT EXISTS SKozhenivskyi;

-- -----------------------------------------------------
-- Table `prods`.`groupsp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SKozhenivskyi`.`groupsp` (
                                                 `id` INT NOT NULL AUTO_INCREMENT,
                                                 `group_name` VARCHAR(20) NOT NULL,
                                                 PRIMARY KEY (`id`));



-- -----------------------------------------------------
-- Table `prods`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SKozhenivskyi`.`products` (
                                                  `id` INT NOT NULL AUTO_INCREMENT,
                                                  `product_name` VARCHAR(50) NOT NULL,
                                                  `groupsp_id` INT NOT NULL,
                                                  PRIMARY KEY (`id`, `groupsp_id`),
                                                  CONSTRAINT `fk_products_groupsp`
                                                      FOREIGN KEY (`groupsp_id`)
                                                          REFERENCES `SKozhenivskyi`.`groupsp` (`id`)
                                                          ON DELETE CASCADE
                                                          ON UPDATE CASCADE );


CREATE INDEX IF NOT EXISTS `fk_products_groupsp_idx` ON `SKozhenivskyi`.`products` (`groupsp_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `prods`.`stores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SKozhenivskyi`.`stores` (
                                                `id` INT NOT NULL AUTO_INCREMENT,
                                                `store` VARCHAR(20) NOT NULL,
                                                PRIMARY KEY (`id`));



-- -----------------------------------------------------
-- Table `prods`.`products_has_stores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SKozhenivskyi`.`products_has_stores` (
                                                             `store_idstore` INT NOT NULL,
                                                             `product_id` INT NOT NULL,
                                                             `count` INT UNSIGNED NOT NULL,
                                                             PRIMARY KEY (`store_idstore`, `product_id`),
                                                             INDEX `fk_store_has_product_product1_idx` (`product_id` ASC) VISIBLE,
                                                             INDEX `fk_store_has_product_store1_idx` (`store_idstore` ASC) VISIBLE,
                                                             CONSTRAINT `fk_store_has_product_store1`
                                                                 FOREIGN KEY (`store_idstore`)
                                                                     REFERENCES `SKozhenivskyi`.`stores` (`id`)
                                                                     ON DELETE CASCADE
                                                                     ON UPDATE CASCADE ,
                                                             CONSTRAINT `fk_store_has_product_product1`
                                                                 FOREIGN KEY (`product_id`)
                                                                     REFERENCES `SKozhenivskyi`.`products` (`id`)
                                                                     ON DELETE CASCADE
                                                                     ON UPDATE CASCADE );



