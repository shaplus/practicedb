insert into groupsp values (default, 'a'), (default, 'b'), (default, 'c');

insert into products values (0, 'prod1', 1);
insert into products values (default, 'prod2', 1);
insert into stores value (default, 'store1');


drop table if exists groupsp;

select * from groupsp;

delete from products_has_stores;
delete from stores;
delete from products;
delete from groupsp;


drop index groupsInd on SKozhenivskyi.products;
select count(product_id) from products_has_stores;
alter table products alter index all on table;

select * from products where groupsp_id = 1;

    set transaction isolation level read uncommitted ;

alter table products AUTO_INCREMENT = 0;

